import React, {Component} from 'react'
import {FlatList, View, TextInput, StyleSheet, TouchableOpacity, Text, Image} from 'react-native'
import PropTypes from 'prop-types'
import ChatMessage from './ChatMessage'

class ChatLog extends Component {
  state = {
    message: '',
    currentUserEmail: '',
    currentUserAvatar: '',
    showUserProfile: false,
  };


  render() {
    const {chats} = this.props;
    const {showUserProfile, currentUserAvatar, currentUserEmail} = this.state;

    const newArr = chats.reduce((prevArr, currentItem) => {
      if (prevArr[prevArr.length-1]?.user_email === currentItem.user_email){
        if (typeof prevArr[prevArr.length-1].message === 'string'){
          prevArr[prevArr.length-1].message = [prevArr[prevArr.length-1].message, currentItem.message ];
        }
        else {
          prevArr[prevArr.length-1].message = [...prevArr[prevArr.length-1].message, currentItem.message ];
        }
        return prevArr;
      }
      return [...prevArr, currentItem];
    }, []);

    return (
      <>
        {showUserProfile ?
          <View style={styles.profileWrapper}>
            <TouchableOpacity style={styles.closeBtnWrapper} onPress={() => this.setState( { showUserProfile: false })}>
              <Text style={styles.closeBtn}> X </Text>
            </TouchableOpacity>
            <Image style={styles.roundedProfileImage} source={{uri: currentUserAvatar}}/>
            <Text style={styles.mailText}>{currentUserEmail}</Text>
          </View>
          :
          ( <>
            <FlatList
              style={{flex: 1, width: '100%'}}
              data={newArr}
              renderItem={(item, index) => this.renderItem(item.item, index)}
              keyExtractor={(item, index) => `${index}`}
            />
            <View style={styles.messagePannel}>
              <TextInput
                style={styles.input}
                multiline={true}
                placeholder="Enter message"
                onChangeText = {this.handleMessage}
              />
              <TouchableOpacity style={styles.btn}>
                <Text style={styles.btnText}> > </Text>
              </TouchableOpacity>
            </View>
          </>)
        }
        </>
    )
  }

  renderItem = (chatMessage, i) => {
        return(<ChatMessage key={`chat${i}`} chatMessage={chatMessage} showUserProfile={this.showUserInfo}/>)

    };

  handleMessage = (text) => {
    this.setState({message: text})
  };

  showUserInfo = (email, avatar) => {
    this.setState({
      currentUserEmail: email,
      currentUserAvatar: avatar,
      showUserProfile: true,
    })
  }
}

const styles = StyleSheet.create({
  profileWrapper: {
    position: 'relative',
    width: '100%',
    alignItems: 'center',
  },
  closeBtn: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  closeBtnWrapper: {
    position: 'absolute',
    right: 0,
  },
  mailText: {
    marginTop: 15 ,
    fontSize: 22,
    fontWeight: 'bold',
  },
  roundedProfileImage: {
    width:120,
    height:120,
    borderWidth:3,
    borderColor:'black',
    borderRadius:60
  },
  messagePannel: {
    width: '100%',
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  btn: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#0000FF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: '#fff',
    fontSize: 25,
    lineHeight: 25,
  },
  input: {
    width: '88%',
    padding: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#727278',
  }

});

ChatLog.propTypes = {
  chats: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string.isRequired,
      user_email: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};

export default ChatLog

