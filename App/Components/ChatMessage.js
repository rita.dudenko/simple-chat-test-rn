import React, {Component} from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import GravatarApi from 'gravatar-api'

class ChatMessage extends Component {

  render() {
    const { chatMessage, showUserProfile } = this.props;


    const avatarUrl = GravatarApi.imageUrl({
      email: chatMessage.user_email,
      parameters: {size: "50", "d": "monsterid"},
    }).replace('http', 'https');

    const isFirstUserEmail = chatMessage.user_email === 'a@b.com';

    return (
      <View style={{...styles.wrapper,
                       flexDirection: isFirstUserEmail ? 'row' : 'row-reverse',
                        alignSelf: isFirstUserEmail ? 'flex-end' : 'flex-start',}}>
        {typeof chatMessage.message === 'string' ?
          ( <Text style={isFirstUserEmail ? styles.rightAlign : styles.leftAlign}>
                {chatMessage.message}
            </Text>
          ) :
          (<View>
            { chatMessage.message.map((item, index) => {
                     return <Text
                               key={`text${index}`}
                               style={[isFirstUserEmail ? styles.rightAlign : styles.leftAlign,
                                      {marginVertical: index % 2 === 0 ? 5 : 0}] }>
                                 {item}
                            </Text>
                } )
            }
          </View>
          )
        }
           <TouchableOpacity onPress={() => showUserProfile(chatMessage.user_email, avatarUrl)}>
             <Image style={styles.roundedProfileImage} source={{uri: avatarUrl}}/>
           </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  roundedProfileImage: {
    width:50,
    height:50,
    borderWidth:3,
    borderColor:'black',
    borderRadius:25
  },
  wrapper: {
    flex: 1,
    alignItems: 'flex-end',
    marginTop: 15,
  },

 rightAlign: {
    maxWidth: '80%',
    marginRight: 4,
    backgroundColor: '#b2b2ff',
    borderRadius: 5,
    padding: 10,

 },
  leftAlign: {
    maxWidth: '80%',
    marginLeft: 4,
    backgroundColor: '#dfdfee',
    borderRadius: 5,
    padding: 10,
  },

});

ChatMessage.propTypes = {
  chatMessage: PropTypes.object.isRequired,
};

export default ChatMessage
