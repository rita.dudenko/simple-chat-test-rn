const defaultState = [
  {
    user_email: 'a@b.com',
    message: 'Hello!',
  },
  {
    user_email: 'c@d.com',
    message: 'Good morning',
  },
  {
    user_email: 'c@d.com',
    message: 'Thank you for booking your appointment. Please arrive 15 mins early to fill out a new patient intake form. Text or call us to reschedule.',
  },
  {
    user_email: 'a@b.com',
    message: 'Tickets are now available for the Corporate Conference. RSVP before June 10 to get a special gift! More details at: ourcompany.com/conference',
  },
  {
    user_email: 'c@d.com',
    message: 'See you soon!',
  },
  {
    user_email: 'a@b.com',
    message: 'Have a nice day!',
  },
];


const chat = (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_MESSAGE':
      let { user_email, message } = action;
      return [
        ...state,
        {
          user_email,
          message,
        }
      ]
    default:
      return state
  }
}

export default chat
